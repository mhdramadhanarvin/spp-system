<?php

	// Ex:
	// Route::get('URL', 'NameController@method'); 				Jika data yang dikirim berbentuk GET
	// Route::post('URL', 'NameController@method');         	Jika data yang dikirim berbentuk post

	// GET
	// Pages
	Route::get('/', 				'HomeController@index');
	Route::get('/login',	 		'AuthController@IndexLogin');
	Route::post('/login-staff',	 	'AuthController@LoginStaff');
	Route::post('/login-student',	'AuthController@LoginStudent');
	Route::get('/logout',	 		'AuthController@Logout');


	//Master Data  
	
	//Staff
	Route::get('/data-staff',		'MasterDataController@staff');
	Route::get('/staff/add',		'StaffController@addIndex');
	Route::get('/staff-edit/{id}',	'StaffController@editIndex');
	Route::get('/staff-delete/{id}','StaffController@deleteProcess');
	Route::post('/staff/add',		'StaffController@addProcess');
	Route::post('/staff-edit/{id}',	'StaffController@editProcess');
	Route::post('/staff-searchh',	'MasterDataController@searchIndexStaff');
	Route::get('/staff-search/{search}',	'MasterDataController@searchProcessStaff');
	
	//Staff
	Route::get('/data-student',		  'MasterDataController@student');
	Route::get('/student/add',		  'StudentController@addIndex');
	Route::get('/student-edit/{id}',  'StudentController@editIndex');
	Route::get('/student-delete/{id}','StudentController@deleteProcess');
	Route::post('/student/add',		  'StudentController@addProcess');
	Route::post('/student-edit/{id}', 'StudentController@editProcess');
	Route::post('/student-searchh',	  'MasterDataController@searchIndexStudent');
	Route::get('/student-search/{search}',	'MasterDataController@searchProcessStudent');
	
	//Class 
	Route::get('/data-class',		'MasterDataController@class');
	Route::get('/class/add',		'ClassController@addIndex');
	Route::get('/class-edit/{id}',  'ClassController@editIndex');
	Route::get('/class-delete/{id}','ClassController@deleteProcess');
	Route::post('/class/add',		'ClassController@addProcess');
	Route::post('/class-edit/{id}', 'ClassController@editProcess');
	Route::post('/class-searchh',	'MasterDataController@searchIndexClass');
	Route::get('/class-search/{search}',	'MasterDataController@searchProcessClass');
	
	//SPP 
	Route::get('/data-spp',			'MasterDataController@spp');
	Route::get('/spp/add',			'SppController@addIndex');
	Route::get('/spp-edit/{id}',  	'SppController@editIndex');
	Route::get('/spp-delete/{id}',	'SppController@deleteProcess');
	Route::post('/spp/add',			'SppController@addProcess');
	Route::post('/spp-edit/{id}', 	'SppController@editProcess');
	Route::post('/spp-searchh',		'MasterDataController@searchIndexSpp');
	Route::get('/spp-search/{search}',	'MasterDataController@searchProcessSpp');
	

	// Transaction
	Route::get('/transactions',		'TransactionController@index');
	Route::get('/history-get/{nisn}',		'PaymentHistoryController@get');
	Route::get('/payment-check/{nisn}',		'PaymentHistoryController@check');