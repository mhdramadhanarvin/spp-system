<?php  

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class TransactionController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->helper(); 
 
		if (!authCheck()) {
			redirect('/logout');
		} else if (isset(Session::get('user')['level']) AND !in_array(Session::get('user')['level'], ['admin', 'staff'])) {
			redirect('/');            
        }

	}  

    public function index()
    {
        $data['title'] = 'Data Staff';
        $id = Session::get('user')['id_staff'];
        $siswa = $this->db->query('
            SELECT * FROM siswa
            LEFT JOIN kelas
            ON kelas.id_kelas = siswa.id_kelas 
        ');
        $siswa = $this->db->resultSet(); 
        $fractal = new Manager();
		$resource = new Collection($siswa, function (array $row) { 
			return [
				'nisn'	=> $row['nisn'],
				'nis'	=> $row['nis'],
				'nama'	=> $row['nama'],
				'alamat'	=> $row['alamat'],
				'no_telp'	=> $row['no_telp'],
				'kelas'	=> $row['nama_kelas'] . '-' . $row['jurusan']
			];
		});
		$data['siswa'] = $fractal->createData($resource)->toArray()['data'];
        $this->view('template/header', $data);
        $this->view('pages/transactions', $data);
		$this->view('template/footer');
    }

}