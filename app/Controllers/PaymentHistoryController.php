<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PaymentHistoryController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->helper();

        if (!authCheck()) {
            redirect('/logout');
        } 
    }

    public function get($nisn)
    {
        $data = $this->db->query("
            SELECT * FROM pembayaran p 
            LEFT JOIN spp
            ON p.id_spp = spp.id
            LEFT JOIN staff
            ON p.id_staff = staff.id_staff
            WHERE p.nisn = 
        ". $nisn. ' ORDER BY tgl_bayar DESC');
        $data = $this->db->resultSet();
        $fractal = new Manager();
		$resource = new Collection($data, function (array $row) {
            $bulanStrx = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            $bulan = str_split($row['bulan']);
            $bulan = $bulan[0] == 0 ? $bulanStrx[$bulan[1] - 1] : $bulanStrx[$row['bulan'] - 1]; 
			return [
				'id_pembayaran'	=> $row['id_pembayaran'],
				'tgl_bayar'	    => $row['tgl_bayar'],
				'jumlah_bayar'	=> 'Rp '.number_format($row['jumlah_bayar'],0 ,',','.'),
				'nama_staff'	=> $row['nama'], 
                'pembayaran'    => $bulan . ' ' . $row['tahun']
			];
		});
		$data = $fractal->createData($resource)->toArray()['data'];
        echo json_encode($data);
    }

    public function check($nisn)
    {

    }
    

}
