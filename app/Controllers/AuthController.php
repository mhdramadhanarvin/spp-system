<?php

class AuthController extends Controller
{

    public function IndexLogin()
    {
        if (Session::check('user') == true) {
            redirect('/');
        } else {
            $this->view('pages/login');
        }
    }

    public function Login()
    {
        $email = $this->post('email');
        $password = $this->post('password');

        if (empty($email) or empty($password)) {
            Flasher::setFlash('<b>Form</b> harus diisi!', 'danger');
            redirect('login');
        } else {
            if ($this->model('Login_Model')->login()) {
                redirect('/');
            } else {
                redirect('login');
            }
        }
    }

    public function LoginStaff()
    {
        $username = $this->post('username');
        $password = $this->post('password'); 
        
        if (empty($username) or empty($password)) {
            Flasher::setFlash('Form harus diisi!', 'danger');
            redirect('/login#signup');
        } else {
            $query = $this->db->table('staff')->where('username', $username);

            if (password_verify($password, $query['password'])) {
                Session::set('user', $query);
                redirect('/');
            } else {
                Flasher::setFlash('Username atau password salah!', 'danger');
                redirect('/login#signup');
            }
        }
    }

    public function LoginStudent()
    {
        $nisn = $this->post('nisn');
        $password = $this->post('password'); 
        
        if (empty($nisn) or empty($password)) {
            Flasher::setFlash('Form harus diisi!', 'danger');
            redirect('/login#signin');
        } else {
            $query = $this->db->table('siswa')->where('nisn', $nisn);
            
            if (password_verify($password, $query['password'])) {
                Session::set('user', $query);
                redirect('/');
            } else {
                Flasher::setFlash('Username atau password salah!', 'danger');
                redirect('/login#signin');
            }
        }
    }

    public function Logout()
    {
        Session::unset();
        redirect('/login');
    }
}
