<?php 

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class MasterDataController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->helper(); 
 
		if (!authCheck()) {
			redirect('/logout');
		} else if (isset(Session::get('user')['level']) AND Session::get('user')['level'] != 'admin') {
			redirect('/');            
        }

	} 
    
    public function staff() 
    {
        $data['title'] = 'Data Staff';
        $id = Session::get('user')['id_staff'];
        $data['rows'] = $this->db->query("SELECT * FROM staff WHERE id_staff != '$id' ");
        $data['rows'] = $this->db->resultSet(); 
        $this->view('template/header', $data);
        $this->view('pages/staff/list', $data);
		$this->view('template/footer');
    }

    public function searchIndexStaff()
    {
        redirect('/staff-search/'. $this->post('search'));
    }
    
    public function searchProcessStaff($search)
    {
        $data['title'] = 'Data Staff';
        $id = Session::get('user')['id_staff']; 
        $data['rows'] = $this->db->query("SELECT * FROM staff WHERE nama LIKE '$search%' AND id_staff != '$id' ");
        $data['rows'] = $this->db->resultSet();
        $data['search'] = $search;
        $this->view('template/header', $data);
        $this->view('pages/staff/list', $data);
        $this->view('template/footer');
    }
    
    public function student() 
    {
        $data['title'] = 'Data Student'; 
        $rows = $this->db->query('
            SELECT * FROM siswa
            LEFT JOIN kelas
            ON kelas.id_kelas = siswa.id_kelas 
        ');
        $rows = $this->db->resultSet(); 
        $fractal = new Manager();
		$resource = new Collection($rows, function (array $row) {
			return [
				'nisn'	=> $row['nisn'],
				'nis'	=> $row['nis'],
				'nama'	=> $row['nama'],
				'alamat'	=> $row['alamat'],
				'no_telp'	=> $row['no_telp'],
				'kelas'	=> $row['nama_kelas'] . '-' . $row['jurusan']
			];
		});
		$data['rows'] = $fractal->createData($resource)->toArray()['data'];
        
        $this->view('template/header', $data);
        $this->view('pages/student/list', $data);
        $this->view('template/footer');       
    }

    public function searchIndexStudent()
    {
        redirect('/student-search/'. $this->post('search'));
    }
    
    public function searchProcessStudent($search)
    {
        $data['title'] = 'Data Student';  
        $rows = $this->db->query("
            SELECT * FROM siswa
            LEFT JOIN kelas
            ON kelas.id_kelas = siswa.id_kelas 
            WHERE nama LIKE '$search%' or nisn LIKE '$search%'  or nis LIKE '$search%'
        ");
        $rows = $this->db->resultSet(); 
        $fractal = new Manager();
		$resource = new Collection($rows, function (array $row) {
			return [
				'nisn'	=> $row['nisn'],
				'nis'	=> $row['nis'],
				'nama'	=> $row['nama'],
				'alamat'	=> $row['alamat'],
				'no_telp'	=> $row['no_telp'],
				'kelas'	=> $row['nama_kelas'] . '-' . $row['jurusan']
			];
		});
		$data['rows'] = $fractal->createData($resource)->toArray()['data'][0];
        $data['search'] = $search;
        $this->view('template/header', $data);
        $this->view('pages/student/list', $data);
        $this->view('template/footer');
    }
    
    public function class() 
    {
        $data['title'] = 'Data Student';  
        $data['rows'] = $this->db->table('kelas')->all();
        $this->view('template/header', $data);
        $this->view('pages/class/list', $data);
        $this->view('template/footer');       
    }
    
    public function spp() 
    {
        $data['title'] = 'Data SPP';  
        $data['rows'] = $this->db->query('SELECT * FROM spp ORDER BY tahun DESC');
        $data['rows'] = $this->db->resultSet();
        // var_dump($data);die;
        $this->view('template/header', $data);
        $this->view('pages/spp/list', $data);
        $this->view('template/footer'); 
    }

}