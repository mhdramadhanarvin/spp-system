<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class StudentController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->helper();

        if (!authCheck()) {
            redirect('/logout');
        } else if (isset(Session::get('user')['level']) and Session::get('user')['level'] != 'admin') {
            redirect('/');
        }
    }

    public function addIndex()
    {
        $data['title'] = 'Add Data Student';
        $classes = $this->db->table('kelas')->all();
        $fractal = new Manager();
		$resource = new Collection($classes, function (array $class) {
			return [
				'id'	=> $class['id_kelas'],
				'name'	=> $class['nama_kelas'] . '-' . $class['jurusan']
			];
		});
		$data['class'] = $fractal->createData($resource)->toArray()['data'];
        
        $this->view('template/header', $data);
        $this->view('pages/student/add', $data);
        $this->view('template/footer');
    }

    public function editIndex($nisn)
    {
        $data['title'] = 'Edit Data Student';
        $data['row'] = $this->db->table('siswa')->where('nisn', $nisn); 
        $classes = $this->db->table('kelas')->all();
        $fractal = new Manager();
		$resource = new Collection($classes, function (array $class) {
			return [
				'id'	=> $class['id_kelas'],
				'name'	=> $class['nama_kelas'] . '-' . $class['jurusan']
			];
		});
		$data['class'] = $fractal->createData($resource)->toArray()['data']; 
        
        $this->view('template/header', $data);
        $this->view('pages/student/edit', $data);
        $this->view('template/footer');
    }

    public function addProcess()
    {
        try {
            $this->db->transaction();
            $nisn = $this->post('nisn');
            $nis = $this->post('nis');
            $nama = $this->post('nama');
            $class = $this->post('class');
            $password = $this->post('password');
            $confirm_password = $this->post('confirm-password');
            $alamat = $this->post('alamat');
            $no_telp = $this->post('no_telp');
            
            $checkSiswa = $this->db->table('siswa')->countRows(['nisn'=>$nisn]); 
            
            if (empty($nisn) or empty($nis) or empty($nama) or empty($class) or empty($password) or empty($confirm_password) or empty($alamat) or empty($no_telp)) 
            {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/student/add');
            } else if ($checkSiswa > 0) {
                Session::set('old', $_POST);
                Flasher::setFlash('NISN sudah digunakan!', 'danger');
                $this->db->rollback();
                redirect('/student/add');
            } else if ($password != $confirm_password) {
                Session::set('old', $_POST);
                Flasher::setFlash('Password konfirmasi tidak sama!', 'danger');
                $this->db->rollback();
                redirect('/student/add');
            } else if (strlen($password) < 8) {
                Session::set('old', $_POST);
                Flasher::setFlash('Password harus lebih dari 8 karakter!', 'danger');
                $this->db->rollback();
                redirect('/student/add');
            } else {
                Session::unset('old');
                Flasher::setFlash('Berhasil menambahkan data!', 'success');
                
                $this->db->table('siswa')->insert([
                    'nisn'  => $nisn,
                    'nis'  => $nis,
                    'nama'  => $nama,
                    'id_kelas'  => $class,
                    'alamat'    => $alamat,
                    'no_telp'    => $no_telp,
                    'password'  => password_hash($password, PASSWORD_DEFAULT)
                ]);
                $this->db->commit();
                redirect('/data-student');
            }

        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/student/add');
        }
    }
    
    public function editProcess($nisn)
    { 
        try {
            $this->db->transaction(); 
            $nis = $this->post('nis');
            $nama = $this->post('nama');
            $class = $this->post('class');
            $password = $this->post('password');
            $confirm_password = $this->post('confirm-password');
            $alamat = $this->post('alamat');
            $no_telp = $this->post('no_telp');
            
            
            if (empty($nis) or empty($nama) or empty($class) or empty($alamat) or empty($no_telp)) 
            {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect("/student-edit/". $nisn);
            } else {
                if (!empty($password)) { //jika password diisi
                    if ($password != $confirm_password) {
                        Session::set('old', $_POST);
                        Flasher::setFlash('Password konfirmasi tidak sama!', 'danger');
                        $this->db->rollback();
                        redirect("/student-edit/". $nisn);
                    } else if (strlen($password) < 8) {
                        Session::set('old', $_POST);
                        Flasher::setFlash('Password harus lebih dari 8 karakter!', 'danger');
                        $this->db->rollback();
                        redirect("/student-edit/". $nisn);
                    } else {
                        Session::unset('old');
                        Flasher::setFlash('Berhasil menambahkan data!', 'success');
                        
                        $this->db->table('siswa')->update([ 
                            'nis'  => $nis,
                            'nama'  => $nama,
                            'id_kelas'  => $class,
                            'alamat'    => $alamat,
                            'no_telp'    => $no_telp,
                            'password'  => password_hash($password, PASSWORD_DEFAULT)
                        ],[
                            'nisn'  => $nisn
                        ]);
                        $this->db->commit();
                        redirect('/data-student');
                    }
                } else { //jika tidak mengisi password
                    Session::unset('old');
                    Flasher::setFlash('Berhasil mengubah data!', 'success');
                    $this->db->table('siswa')->update([ 
                        'nis'  => $nis,
                        'nama'  => $nama,
                        'id_kelas'  => $class,
                        'alamat'    => $alamat,
                        'no_telp'    => $no_telp
                    ],[
                        'nisn'  => $nisn
                    ]);
                    $this->db->commit();
                    redirect('/data-student');
                }
            }
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect("/student-edit/". $nisn);
        }
    }

    public function deleteProcess($id)
    {
        try {
            $this->db->transaction();
            $this->db->table('siswa')->delete(['nisn'=>$id]);
            $this->db->commit();
            Flasher::setFlash('Berhasil menghapus data!', 'success');
            redirect('/data-student');
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/data-student');
        } 
    }
}