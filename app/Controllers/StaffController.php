<?php



class StaffController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->helper();

        if (!authCheck()) {
            redirect('/logout');
        } else if (isset(Session::get('user')['level']) and Session::get('user')['level'] != 'admin') {
            redirect('/');
        }
    }

    public function addIndex()
    {
        $data['title'] = 'Add Data Staff';
        $this->view('template/header', $data);
        $this->view('pages/staff/add');
        $this->view('template/footer');
    }

    public function editIndex($id)
    {
        $data['title'] = 'Edit Data Staff';
        $data['row'] = $this->db->table('staff')->where('id_staff', $id);  
        $this->view('template/header', $data);
        $this->view('pages/staff/edit', $data);
        $this->view('template/footer');
    }

    public function addProcess()
    {
        try {
            $this->db->transaction();
            
            $name = $this->post('name');
            $username = $this->post('username');
            $password = $this->post('password');
            $password = $this->post('password');
            $confirm_password = $this->post('confirm-password');

            if (empty($name) or empty($username) or empty($password) or empty($confirm_password)) {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/staff/add');
            } else {
                $check = $this->db->table('staff')->countRows(['username'=>$username]); 
                if ($password != $confirm_password) {
                    Session::set('old', $_POST);
                    Flasher::setFlash('Password konfirmasi tidak sama!', 'danger');
                    $this->db->rollback();
                    redirect('/staff/add');
                } else if (strlen($password) < 8) {
                    Session::set('old', $_POST);
                    Flasher::setFlash('Password harus lebih dari 8 karakter!', 'danger');
                    $this->db->rollback();
                    redirect('/staff/add');
                } else if ($check > 0) {
                    Session::set('old', $_POST);
                    Flasher::setFlash('Username sudah digunakan!', 'danger');
                    $this->db->rollback();
                    redirect('/staff/add');
                } else {
                    Session::unset('old');
                    Flasher::setFlash('Berhasil menambahkan data!', 'success');
                    $this->db->table('staff')->insert([
                        'username'  => $username,
                        'nama'  => $name,
                        'password'  => password_hash($password, PASSWORD_DEFAULT),
                        'level'     => 'staff'
                    ]);
                    $this->db->commit();
                    redirect('/data-staff');
                }
            }
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/staff/add');
        }
    }

    public function editProcess($id)
    {
        try {
            $this->db->transaction();
            $name = $this->post('name');
            $username = $this->post('username');
            $password = $this->post('password');
            $confirm_password = $this->post('confirm-password');
    
            if (empty($name) or empty($username)) {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/staff-edit/' . $id);
            } else {
                if (!empty($password)) { //jika password diisi
                    if ($password != $confirm_password) {
                        Session::set('old', $_POST);
                        Flasher::setFlash('Password konfirmasi tidak sama!', 'danger');
                        $this->db->rollback();
                        redirect('/staff-edit/'. $id);
                    } else if (strlen($password) < 8) {
                        Session::set('old', $_POST);
                        Flasher::setFlash('Password harus lebih dari 8 karakter!', 'danger');
                        $this->db->rollback();
                        redirect('/staff-edit/'. $id);
                    } else {
                        Session::unset('old');
                        Flasher::setFlash('Berhasil mengubah data!', 'success');
                        $this->db->table('staff')->update([
                            'nama'  => $name,
                            'username'  => $username,
                            'password'  => password_hash($password, PASSWORD_DEFAULT)
                        ], [
                            'id_staff' => $id
                        ]);
                        $this->db->commit();
                        redirect('/data-staff');
                    }
                } else { //jika tidak mengisi password
                    Session::unset('old');
                    Flasher::setFlash('Berhasil mengubah data!', 'success');
                    $this->db->table('staff')->update([
                        'nama'  => $name,
                        'username'  => $username
                    ], [
                        'id_staff' => $id
                    ]);
                    $this->db->commit();
                    redirect('/data-staff');
                }
            }
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/staff-edit/'. $id);
        }
    }

    public function deleteProcess($id)
    {
        try {
            $this->db->transaction();
            $this->db->table('staff')->delete(['id_staff'=>$id]);
            $this->db->commit();
            Flasher::setFlash('Berhasil menghapus data!', 'success');
            redirect('/data-staff');
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/data-staff');
        } 
    }
}
