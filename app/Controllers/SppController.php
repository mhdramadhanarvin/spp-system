<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SppController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->helper();

        if (!authCheck()) {
            redirect('/logout');
        } else if (isset(Session::get('user')['level']) and Session::get('user')['level'] != 'admin') {
            redirect('/');
        }
    }

    public function addIndex()
    {
        $data['title'] = 'Add Data SPP';
        $data['spp'] = $this->db->table('spp')->all();

        $this->view('template/header', $data);
        $this->view('pages/spp/add', $data);
        $this->view('template/footer');
    }

    public function editIndex($id)
    {
        $data = $this->db->table('spp')->where('id', $id);

        echo json_encode($data);
    }

    public function addProcess()
    {
        try {
            $this->db->transaction();
            $tahun = $this->post('tahun');
            $bulan = $this->post('bulan');

            if (empty($tahun) or empty($bulan)) {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/spp/add');
            } else {
                Session::unset('old');
                Flasher::setFlash('Berhasil menambahkan data!', 'success');

                $this->db->table('spp')->insert([
                    'tahun'  => $tahun,
                    'bulan'  => $bulan,
                ]);
                $this->db->commit();
                redirect('/data-spp');
            }
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/spp/add');
        }
    }

    public function editProcess($id)
    {
        try {
            $this->db->transaction();
            $tahun = $this->post('tahun');
            $bulan = $this->post('bulan');
 
            Flasher::setFlash('Berhasil menambahkan data!', 'success');

            $this->db->table('spp')->update([
                'tahun'  => $tahun,
                'bulan'  => $bulan,
            ], [
                'id'    => $id
            ]);
            $this->db->commit(); 
            echo json_encode([
                'message'   => 'OK'
            ]);
        } catch (\Exception $e) {
            $this->db->rollback(); 
            echo json_encode([
                'message'   => $e->getMessage()
            ]);
        }
    }

    public function deleteProcess($id)
    {
        try {
            $this->db->transaction();
            $this->db->table('spp')->delete(['id' => $id]);
            $this->db->commit();
            Flasher::setFlash('Berhasil menghapus data!', 'success');
            redirect('/data-spp');
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/data-spp');
        }
    }
}
