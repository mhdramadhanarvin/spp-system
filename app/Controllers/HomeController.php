<?php 



class HomeController extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->helper(); 
 
		if (!authCheck()) {
			redirect('/logout');
		} 

	} 

    public function index()
    { 
		$this->view('template/header');
		$this->view('pages/dashboard');
		$this->view('template/footer');
    }

    public function error()
    {
        $error = new Exception('Error System');
        echo $error->getMessage();
    }

}