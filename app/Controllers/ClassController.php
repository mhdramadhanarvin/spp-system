<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ClassController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->helper();

        if (!authCheck()) {
            redirect('/logout');
        } else if (isset(Session::get('user')['level']) and Session::get('user')['level'] != 'admin') {
            redirect('/');
        }
    }

    public function addIndex()
    {
        $data['title'] = 'Add Data Kelas'; 
        
        $this->view('template/header', $data);
        $this->view('pages/class/add');
        $this->view('template/footer');
    }

    public function editIndex($id_kelas)
    {
        $data['title'] = 'Edit Data Student';
        $data['row'] = $this->db->table('kelas')->where('id_kelas', $id_kelas);  
        
        $this->view('template/header', $data);
        $this->view('pages/class/edit', $data);
        $this->view('template/footer');
    }

    public function addProcess()
    {
        try {
            $this->db->transaction();
            $nama_kelas = $this->post('nama_kelas');
            $jurusan = $this->post('jurusan');  
            
            if (empty($nama_kelas) or empty($jurusan)) 
            {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/class/add');
            } else {
                Session::unset('old');
                Flasher::setFlash('Berhasil menambahkan data!', 'success');
                
                $this->db->table('kelas')->insert([
                    'nama_kelas'  => $nama_kelas,
                    'jurusan'  => $jurusan, 
                ]);
                $this->db->commit();
                redirect('/data-class');
            }

        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/class/add');
        }
    }
    
    public function editProcess($id_kelas)
    { 
        try {
            $this->db->transaction();
            $nama_kelas = $this->post('nama_kelas');
            $jurusan = $this->post('jurusan');  
            
            if (empty($nama_kelas) or empty($jurusan)) 
            {
                Session::set('old', $_POST);
                Flasher::setFlash('Form tidak boleh kosong!', 'danger');
                $this->db->rollback();
                redirect('/class/add');
            } else {
                Session::unset('old');
                Flasher::setFlash('Berhasil mengubah data!', 'success');
                
                $this->db->table('kelas')->update([
                    'nama_kelas'  => $nama_kelas,
                    'jurusan'  => $jurusan, 
                ], [
                    'id_kelas'  => $id_kelas
                ]);
                $this->db->commit();
                redirect('/data-class');
            }

        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/class/add');
        }
    }

    public function deleteProcess($id)
    {
        try {
            $this->db->transaction();
            $this->db->table('kelas')->delete(['id_kelas'=>$id]);
            $this->db->commit();
            Flasher::setFlash('Berhasil menghapus data!', 'success');
            redirect('/data-class');
        } catch (\Exception $e) {
            $this->db->rollback();
            Session::set('old', $_POST);
            Flasher::setFlash($e->getMessage() . '!', 'danger');
            redirect('/data-class');
        } 
    }
}