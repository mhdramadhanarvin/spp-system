<?php

	/**
	 * 
	 */
	class App
	{	
		protected $controller = CONTROLLER;
		protected $method = METHOD;
		protected $params = [];
		public function __construct()
		{   	
			// var_dump($_GET);die; 
			$url = $this->parseURL(); 

			//controller
			if (file_exists('app/Controllers/' . $url[0] . 'Controller.php')) {
				$this->controller = $url[0] . 'Controller';
				unset($url[0]);  
			} 

			require_once 'app/Controllers/' . $this->controller . '.php'; 
			$this->controller = new $this->controller;

		
			// method  
			if (isset($url[1])) {
				if (method_exists($this->controller, $url[1])) {
					$this->method = $url[1];
					unset($url[1]);
				}
			}


			//params
			if (!empty($url)) {
				$this->params = array_values($url);
			} 


			//jalankan controller dan method , serta kirim params
			call_user_func_array([$this->controller, $this->method], $this->params);

		}

		public function parseURL() 
		{ 
			if (isset($_GET['uri'])) {
				$url = rtrim($_GET['uri'], '/');
				$url = filter_var($url, FILTER_SANITIZE_URL);
				$url = explode('/', $url);  
			}   
			return $url;
		}    

		
	} 


	class Route extends Config
	{ 
		public static $controller = CONTROLLER;
		public static $method = METHOD;
		public static $params;  

		public static $get,$path,$paths,$count,$counts;  

		public static function get ($url, $controller, $params = null)
		{          
			$BASEURL = (new self)->BASEURL;  
			if ($_SERVER['REQUEST_METHOD'] == 'GET'){ 
				static::$path = explode('/', $_SERVER['REQUEST_URI']);
				static::$path = str_replace(explode('/', $BASEURL), '', static::$path); 
				
				foreach (static::$path as $key => $value) {
					
					if (empty($value)) { 
						unset(static::$path[$key]); 
					}
				} 
				static::$path = implode('/', static::$path);  
				static::$path = explode('/', static::$path);
				
				array_unshift(static::$path, $BASEURL);
				static::$paths = static::$path;
				if (isset(static::$path[2])) {
					// [0] = $BASEURL
					// [1] = uri1
					// [2] = uri2
					
					static::$get = explode('/', $url);
				 	
					if (count(static::$get) > 2) {
						if (preg_match('/{/', static::$get[2]) > 0) {
							
							$count = count(static::$path) ;  
							if ($count > 3) {
								$count = $count - 1;
							} else {
								$count = $count;
							} 
							for($i=1;$i<$count;$i++) {
								array_shift(static::$paths);
							}  
							 
							static::$params = implode('/', static::$paths); 
							$url = '/' . static::$get[1];
						} else { 
							static::$paths = explode('/', $_SERVER['REQUEST_URI']);
							static::$paths = str_replace(explode('/', $BASEURL), '', static::$paths);
							foreach (static::$paths as $key => $value) { 
								if (empty($value)) { 
									unset(static::$paths[$key]); 
								}
							}   
							static::$path[1] = implode('/', static::$paths); 
						}

					}
					
				}  
				static::$path[1] = '/'.static::$path[1]; 
 
				$_SERVER['REQUEST_PATH'] = static::$path[1];  
				if (static::$path[1] == $url) { 
					$url = explode('Controller@', $controller); 

					static::$controller = $url[0]; 
					static::$method = $url[1];
					unset($url[0]);
					unset($url[1]);
				} 
				return rtrim( $_GET['uri'] = static::$controller . '/' . static::$method . '/' . static::$params , '/') ; 
			} 
		} 

		//metode POST
		public static function post ($url, $controller, $params = null)
		{  
			$BASEURL = (new self)->BASEURL;   
			if ($_SERVER['REQUEST_METHOD'] == 'POST'){ 

				static::$path = explode('/', $_SERVER['REQUEST_URI']);
				static::$path = str_replace(explode('/', $BASEURL), '', static::$path); 
				
				foreach (static::$path as $key => $value) {
					
					if (empty($value)) { 
						unset(static::$path[$key]); 
					}
				} 
				static::$path = implode('/', static::$path);  
				static::$path = explode('/', static::$path);
				
				array_unshift(static::$path, $BASEURL);
				static::$paths = static::$path;
				if (isset(static::$path[2])) {
					// [0] = $BASEURL
					// [1] = uri1
					// [2] = uri2
					
					static::$get = explode('/', $url); 

					if (count(static::$get) > 2) {
						if (preg_match('/{/', static::$get[2]) > 0) {
							
							$count = count(static::$path) ;  
							if ($count > 3) {
								$count = $count - 1;
							} else {
								$count = $count;
							} 
							for($i=1;$i<$count;$i++) {
								array_shift(static::$paths);
							}   
							static::$params = implode('/', static::$paths); 
							$url = '/' . static::$get[1];
						} else { 
							static::$paths = explode('/', $_SERVER['REQUEST_URI']);
							static::$paths = str_replace(explode('/', $BASEURL), '', static::$paths);
							foreach (static::$paths as $key => $value) { 
								if (empty($value)) { 
									unset(static::$paths[$key]); 
								}
							}   
							static::$path[1] = implode('/', static::$paths); 
						}
					}
					
				}  
				static::$path[1] = '/'.static::$path[1]; 
 
				$_SERVER['REQUEST_PATH'] = static::$path[1];  
				if (static::$path[1] == $url) { 
					$url = explode('Controller@', $controller); 

					static::$controller = $url[0]; 
					static::$method = $url[1];
					unset($url[0]);
					unset($url[1]);
				} 
				return rtrim( $_GET['uri'] = static::$controller . '/' . static::$method . '/' . static::$params , '/') ; 
			}
		}  

		public function Config()
		{ 
		}
	} 



	class Config 
	{
		public $BASEURL,$PATH_CDN;
		public function __construct()
		{   
			$this->BASEURL = BASEURL; 
			$this->PATH_CDN = PATH_CDN;  
		}

		public function path($name)
		{
			$this->db = new Database;  
			$path = $this->db->table(__CONFIG)->where('name', $name);
			return $path['content'];
		}
	}

	function redirect($url)
	{
		$config = new Config; 
		header("Location:". $config->BASEURL . $url, true, 301);
		die();
	} 

	function url($path = null)
	{
		$config = new Config;
		if (empty($path) || is_null($path)) { 
			return $config->BASEURL;
		} else {
			return $config->BASEURL.'/'.$path;
		}
	}

	function cdn($path)
	{
		$config = new Config;
		return $config->PATH_CDN.$path;
	}

	function asset($path)
	{
		$config = new Config;
		return $config->BASEURL.'/public/'.$path;
	}

	function path($columns)
	{
		$config = new Config;
		$path = $config->path($columns);
		
		return $config->BASEURL.'/'.$path;
	}

	function paths($columns)
	{
		$config = new Config;
		$path = $config->path($columns);
		
		return $path;
	} 