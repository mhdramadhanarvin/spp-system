<?php

class Controller
{

    public function __construct()
    {
        $this->db = new Database;
    }

    public function view($view, $data = [])
    {
        require_once 'app/Views/' . $view . '.php';
    }

    public function model($model)
    {
        require 'app/Models/' . $model . '.php';
        return new $model;
    }

    public function helper()
    {
        require_once 'app/Core/Helper.php';
    }

    public function set_session($name, $data)
    {
        session_start();
        $_SESSION[$name] = $data;
        return true;
    }

    public function is_session($name)
    {
        if (isset($_SESSION[$name])) {
            return true;
        } else {
            return false;
        }
    }

    public function is_login()
    {
        if ($this->is_session('users')) {
            return true;
        } else {
            return false;
        }
    }

    public function error()
    {
        $error = new Exception('Error System');
        echo $error->getMessage();
    }

    public function post($name, $type = null)
    {
        if (!empty($_POST)) {
            switch ($type) {
                case FALSE:
                    return htmlspecialchars(addslashes($_POST[$name]));
                    break;

                case TRUE:
                    return htmlspecialchars(htmlentities(trim($_POST[$name]), ENT_QUOTES, 'UTF-8'));
                    break;

                default:
                    return htmlspecialchars(htmlentities(trim($_POST[$name]), ENT_QUOTES, 'UTF-8'));
                    break;
            }
        } else {
            $data = file_get_contents("php://input");
            $data = json_decode($data);
            return $data->$name;
        }
    }
    public function buatKode($tabel, $inisial, $field)
    {
        $qry    = mysqli_query($this->db->connection(), "SELECT MAX(" . $field . ") FROM " . $tabel);
        $row    = mysqli_fetch_array($qry);


        if ($row['0'] == "") {
            $inisials         = $inisial . "0001";
            return $hasilkode = $inisials;
        } else {
            $nilaikode    = substr($row[0], strlen($inisial));

            $kode         = (int) $nilaikode;

            $kode         = $kode + 1;

            return $hasilkode = $inisial . str_pad($kode, 4, "0", STR_PAD_LEFT);
        }
    }

    public function sendMail($type, $data)
    {
        $this->mail = new PHPMailer;
        $this->mail->IsSMTP();
        $this->mail->SMTPSecure = 'tls';
        // $this->mail->Host = "gaming.stream-universe.id"; //host masing2 provider email
        $this->mail->Host = "smtp.gmail.com"; //host masing2 provider email
        $this->mail->SMTPDebug = 1;
        // $this->mail->Port = 465;
        $this->mail->Port = 587;
        $this->mail->SMTPAuth = true;
        // $this->mail->Username = "support@gaming.stream-universe.id"; //user email
        $this->mail->Username = "noreply.streamgaming@gmail.com"; //user email
        $this->mail->Password = "wfsrfnctuxzcrumf"; //password email 
        $this->mail->SetFrom("noreply@streamgaming.id", "Stream Gaming"); //set email pengirim
        $this->mail->AddAddress($data['email'], "");
        $this->mail->IsHTML(true);

        ob_start();
        if ($type == 'reset-pin-user') {
            $subjek = "Please Change Your New PIN";
            $this->view('email/reset-pin', $data);
        } elseif ($type == 'reset-pass-management') {
            $subjek = "Please Change Your New Password";
            $this->view('email/reset-password', $data);
        } elseif ($type == 'new-management') {
            $subjek = "New Account Portal Stream Gaming";
            $this->view('email/new-management', $data);
        } elseif ($type == "leaderboard") {
            $subjek = "[Information] New Tournament has just been released!";
            $data['link_banner'] = cdn(paths('backup_banner_tournament')) . $data["banner"];
            $data['tour_link'] = 'https://streamgaming.id/leaderboard/' . $data['url'];
            $this->view('email/email-newtournament', $data);
        } elseif ($type == "Bracket") {
            $subjek = "[Information] New Tournament has just been released!";
            $data['link_banner'] = cdn(paths('backup_banner_tournament')) . $data["banner"];
            $data['tour_link'] = 'https://streamgaming.id/tournament/' . $data['url'];
            $this->view('email/email-bracket', $data);
        }
        $content = ob_get_contents();
        ob_end_clean();
        $this->mail->Subject = $subjek;
        $this->mail->Body = $content;

        if ($this->mail->Send()) return true;
        else return false;
    }
}
