<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stream - Universe</title>

    <!-- Custom fonts for this template--> 
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= cdn('v2/assets/css/sb-admin-2.min.css');?>" rel="stylesheet">
    <link href="<?= cdn('v2/assets/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet">

</head>
<style>
    body {
        position: relative; 
        background: linear-gradient(132deg, #f44336, #E91E63, #9C27B0, #673AB7, #3F51B5, #2196F3,#03A9F4, #00BCD4, #009688, #4CAF50, #FFC107, #FF9800, #f44336, #E91E63, #9C27B0, #673AB7, #3F51B5, #2196F3,#03A9F4, #00BCD4, #009688, #4CAF50, #FFC107, #FF9800);
        background-size: 400% 400%;
        animation: BackgroundGradient 30s ease infinite; 
    }

    @keyframes BackgroundGradient {
        0% {background-position: 0% 50%;}
        50% {background-position: 100% 50%;}
        100% {background-position: 0% 50%;}
    }

    #particles-js{
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: 50% 50%;
        position: fixed;
        top: 0px;
        z-index:1;
    }
    .card{
        background-color: rgba(255, 255, 255, 0.3) !important;
        z-index: 9999;
    }
</style>

<body>

    <div id="particles-js"></div>
    <div class="container">

        <!-- Outer Row -->

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10 col-sm-12 text-center"> 
                <img src="<?= url('public/assets_login/images/streamgaming.png');?>" class="img-login" width="65%" alt="">
            </div>
        </div>
        <div class="row justify-content-center mb-5">

            <div class="col-sm-12 col-lg-6 col-md-10">

                <div class="card o-hidden border-primary shadow-lg">
                    <div class="card-body">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h2 text-primary mb-4"><b>Welcome Back! </b></h1>

                                        <div class="col-lg-12">
                                            <?php Flasher::flash();?>
                                        </div>
                                    </div>
                                    <form class="user" action="<?= url('login') ?>" method="post" >

                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" name="email" id="exampleInputEmail" placeholder="Username / Email" autofocus required autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" name="password" id="password-field" placeholder="Password" autocomplete="off" required>
                                            <span style="float:right;margin-top:-33px;margin-right:25px;font-size:18px;" toggle="#password-field" class="toggle-password fas fa-eye"></span>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label text-dark" for="customCheck">Remember Me</label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block"> Login </button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div> 
        </div> 
    </div>



    <!-- Bootstrap core JavaScript-->
    <script src="<?= cdn('v2/assets/vendor/jquery/jquery.min.js');?>"></script>
    <script src="<?= cdn('v2/assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script> 

    <!-- Core plugin JavaScript-->
    <script src="<?= cdn('v2/assets/vendor/jquery-easing/jquery.easing.min.js');?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= cdn('v2/assets/js/sb-admin-2.min.js');?>"></script>
    <script>
        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js", function(){
            particlesJS('particles-js',
            {
                "particles": {
                    "number": {
                        "value": 80,
                        "density": {
                            "enable": true,
                            "value_area": 600
                        }
                    },
                    "color": {
                        "value": "#FFFF00"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#FF0000"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 5,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#7CFC00",
                        "opacity": 0.5,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 6,
                        "direction": "none",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 100
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            }
            ); 
        });
    </script>
    <script>
        <?php if (Session::check('_login_again')):?>
        // Set the date we're counting down to
        var countDownDate = new Date("<?=date('M d, Y H:i:s', strtotime(Session::get('_login_again'))) ?>").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();
            var date = new Date();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            if (now > countDownDate) {
                document.getElementById("timer").innerHTML = "NOW";
            } else {
                document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";
            }

            // If the count down is over, write some text 
            if (distance < 0) {
                clearInterval(x); 
            }
        }, 1000);
        <?php endif;?>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        }); 
    </script>
</body> 
</html>
