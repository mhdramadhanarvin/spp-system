<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Transaksi Pembayaran SPP</small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="col-md-6 col-sm-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Siswa</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th class="column-title">NISN </th>
                                        <th class="column-title">NIS </th>
                                        <th class="column-title">Nama </th>
                                        <th class="column-title">Kelas </th>
                                        <th class="column-title">Action </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($data['siswa'] as $siswa) : ?>
                                        <tr class="even pointer">
                                            <td class="a-center "><?= $i++; ?></td>
                                            <td class="a-center "><?= $siswa['nisn']; ?></td>
                                            <td class="a-center "><?= $siswa['nis']; ?></td>
                                            <td class="a-center "><?= $siswa['nama']; ?></td>
                                            <td class="a-center "><?= $siswa['kelas']; ?></td>
                                            <td class=" last">
                                                <a href="javascript:void(0)" onclick="payCheck('<?= $siswa['nisn']; ?>')" data-toggle="modal" data-target=".pay">Pembayaran</a> |
                                                <a href="javascript:void(0)" onclick="getHistory('<?= $siswa['nisn']; ?>')">Riwayat</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Riwayat Pembayaran</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content content-history">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th class="column-title">Tanggal Bayar </th>
                                        <th class="column-title">Untuk Pembayaran </th>
                                        <th class="column-title">Nominal </th>
                                        <th class="column-title">Status </th>
                                        <th class="column-title">Staff Pembayaran </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="even pointer">
                                        <td class="a-center text-center" colspan="6">Belum ada data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content --> 
<div class="modal fade pay" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form id="demo-form2" action="<?= url('/spp/add'); ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Tambah Data Pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 label-align" for="bulan">NISN <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <input type="text" id="first-name" required="required" class="form-control col-md-10">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 label-align" for="bulan">NIS <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <input type="text" id="first-name" required="required" class="form-control col-md-10">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 label-align" for="bulan">Nama <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <input type="text" id="first-name" required="required" class="form-control col-md-10">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tahun">Untuk Pembayaran <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <select class="form-control col-md-10" name="tahun" required="required">
                                <option value="">Pilih Tahun</option>
                                <?php for ($tahun = date('Y') - 3; $tahun < date('Y') + 3; $tahun++) : ?>
                                    <option value="<?= $tahun; ?>"><?= $tahun; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 label-align" for="bulan">Nominal <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <input type="number" id="first-name" required="required" class="form-control col-md-10">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function getHistory(nisn)
    { 
        console.log(nisn)
        fetch('<?= url('history-get/'); ?>' + nisn)
        .then((res) => res.json())
        .then((data) => {
            if (data.length > 0) {
                $('.content-history tbody').html(''); 
                $.each(data, function (index, value) {
                    $('.content-history tbody').append(`
                        <tr class="even pointer">
                            <td class="a-center">${index+1}</td>
                            <td class="a-center">${value.tgl_bayar}</td>
                            <td class="a-center">${value.pembayaran}</td>
                            <td class="a-center">${value.jumlah_bayar}</td>
                            <td class="a-center"><span class="badge badge-success">LUNAS</span></td>
                            <td class="a-center">${value.nama_staff}</td>  
                        </tr>
                    `);
                });
            } else {
                $('.content-history tbody').html(''); 
                $('.content-history tbody').append(`
                        <tr class="even pointer">
                            <td class="a-center text-center" colspan="6">Belum ada data</td>
                        </tr>
                    `);
            }
        })
        .catch((error) => console.log(error)) 
    }

    function payCheck(nisn)
    {
        fetch('<?= url('payment-check/'); ?>' + nisn)
        .then((res) => res.json())
        .then((data) => { 
        })
        .catch((error) => console.log(error)) 
    }
</script>