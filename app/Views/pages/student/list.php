<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Data Siswa</small></h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                    <form action="<?=url('student-searchh'); ?>" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?= $data['search'] ?? ''; ?>" name="search" placeholder="Cari disini..." require="required">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Cari</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <a href="<?= url('student/add'); ?>" class="btn btn-success mt-2">Tambah Data</a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <?= Flasher::flash(); ?>
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th class="column-title">NISN </th>
                                        <th class="column-title">NIS </th>
                                        <th class="column-title">Nama </th>
                                        <th class="column-title">Kelas </th>
                                        <th class="column-title">Alamat </th>
                                        <th class="column-title">No. Telp </th>
                                        <th class="column-title">Action </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($data['rows'] as $row) : ?>
                                        <tr class="even pointer">
                                            <td class="a-center "><?= $i++; ?></td>
                                            <td class=" "><?=$row['nisn'];?></td>
                                            <td class=" "><?=$row['nis'];?></td>
                                            <td class=" "><?=$row['nama'];?></td>
                                            <td class=" "><?=$row['kelas'];?></td>
                                            <td class=" "><?=$row['alamat'];?></td>
                                            <td class=" "><?=$row['no_telp'];?></td>
                                            <td class=" last">
                                                <a href="<?= url('student-edit/' . $row['nisn']); ?>">Edit</a> |
                                                <a href="<?= url('student-delete/' . $row['nisn']); ?>" onclick="return confirm('Yakin ingin menghapus?')">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->