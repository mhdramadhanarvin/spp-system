<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left pb-2">
                <h3>Ubah Data Siswa</small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                        <div class="x_content">
                            <?= Flasher::flash(); ?>
                            <form id="demo-form2" action="" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nisn">NISN <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="number" id="nisn" value="<?= Session::get('old')['nisn'] ?? $data['row']['nisn']; ?>" class="form-control disabled" disabled="disabled" style="cursor:not-allowed">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nis">NIS <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="number" id="nis" name="nis" value="<?= Session::get('old')['nis'] ?? $data['row']['nis'];; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Nama <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="text" id="nama" name="nama" value="<?= Session::get('old')['nama'] ?? $data['row']['nama']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Kelas <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <select class="form-control" name="class">
                                            <option value="">Pilih...</option>
                                            <?php foreach ($data['class'] as $class):
                                                if (Session::check('old')) {
                                                    $selected = $class['id'] == Session::get('old')['class'] ? 'selected' : '';
                                                } else {
                                                    $selected = $class['id'] == $data['row']['id_kelas'] ? 'selected' : '';
                                                } 
                                            ?>
                                            <option value="<?=$class['id'];?>" <?=$selected?>><?=$class['name'];?></option> 
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="password">Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="password" id="password" name="password" class="form-control">
                                        <small>Note: Isi jika ingin mengubah password</small>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="confirm-password">Konfirmasi Password <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="password" id="confirm-password" name="confirm-password" class="form-control">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="alamat">Alamat <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <textarea id="alamat" name="alamat" class="form-control"><?= Session::get('old')['alamat'] ?? $data['row']['alamat']; ?></textarea>
                                    </div>
                                </div><div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="no_telp">No HP <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="text" id="no_telp" name="no_telp" value="<?= Session::get('old')['no_telp'] ?? $data['row']['no_telp']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="item form-group">
                                    <div class="col-md-6 col-sm-6 offset-md-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?= url('data-student'); ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->