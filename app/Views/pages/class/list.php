<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Data Kelas</small></h3>
            </div> 
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <a href="<?= url('class/add'); ?>" class="btn btn-success mt-2">Tambah Data</a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <?= Flasher::flash(); ?>
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th class="column-title">Nama Kelas </th>
                                        <th class="column-title">Jurusan </th> 
                                        <th class="column-title">Action </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($data['rows'] as $row) : ?>
                                        <tr class="even pointer">
                                            <td class="a-center "><?= $i++; ?></td>
                                            <td class=" "><?=$row['nama_kelas'];?></td>
                                            <td class=" "><?=$row['jurusan'];?></td> 
                                            <td class=" last">
                                                <a href="<?= url('class-edit/' . $row['id_kelas']); ?>">Edit</a> |
                                                <a href="<?= url('class-delete/' . $row['id_kelas']); ?>" onclick="return confirm('Yakin ingin menghapus?')">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->