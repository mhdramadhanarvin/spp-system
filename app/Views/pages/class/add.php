<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left pb-2">
                <h3>Tambah Data Kelas</small></h3>
            </div> 
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel"> 
                        <div class="x_content">
                            <?=Flasher::flash();?>
                            <form id="demo-form2" action="" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama_kelas">Nama Kelas <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="text" id="nama_kelas" name="nama_kelas" value="<?=Session::get('old')['nama_kelas'] ?? "";?>" class="form-control ">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="jurusan">Jurusan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 ">
                                        <input type="text" id="jurusan" name="jurusan" value="<?=Session::get('old')['jurusan'] ?? "";?>" class="form-control">
                                    </div>
                                </div> 
                                <div class="ln_solid"></div>
                                <div class="item form-group">
                                    <div class="col-md-6 col-sm-6 offset-md-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?=url('data-staff');?>" class="btn btn-danger">Cancel</a> 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->