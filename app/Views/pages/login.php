<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login Page</title>

    <!-- Bootstrap -->
    <link href="public/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="public/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="public/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="public/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="public/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                <form action="<?= url('login-student'); ?>" method="post"> 
                        <h1>Login as Student</h1>
                        <?php Flasher::flash(); ?>
                        <div>
                            <input type="text" class="form-control" name="nisn" placeholder="NISN" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                        </div>
                        <div>
                        <button type="submit" class="btn btn-primary submit">Log In</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">If you are staff?
                                <a href="#signup" class="to_register"> Login Here </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1>Sistem Pembayaran SPP!</h1>
                                <p>©2021 Mayang Suri.</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>

            <div id="register" class="animate form registration_form">
                <section class="login_content">
                    <form action="<?= url('login-staff'); ?>" method="post"> 
                        <h1>Login as Staff</h1>
                        <?php Flasher::flash(); ?>
                        <div>
                            <input type="text" class="form-control" name="username" placeholder="Username"  />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password"  />
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary submit">Log In</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">If you are student ?
                                <a href="#signin" class="to_register"> Login Here </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1>Sistem Pembayaran SPP!</h1>
                                <p>©2021 Mayang Suri.</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>

</html>