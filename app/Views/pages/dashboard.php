<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
            <div class="col-md-3 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Siswa</span>
                <div class="count green">1500</div>
                <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-3 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Staff</span>
                <div class="count">125</div>
                <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>
            <div class="col-md-3 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Kelas</span>
                <div class="count">2,500</div>
                <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-3 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Belum Membayar SPP</span>
                <div class="count red">4,567</div> 
                <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div> 
        </div>
    </div>
    <!-- /top tiles -->
    
    <div class="row text-center">
        <h3> Selamat Datang, <?=Session::get('user')['nama'];?></h3>
    </div>
</div>