<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>List Data SPP</small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <button href="#" class="btn btn-success mt-2" data-toggle="modal" data-target=".modal-add">Tambah Data</button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <?= Flasher::flash(); ?>
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th class="column-title">Bulan </th>
                                        <th class="column-title">Tahun </th>
                                        <th class="column-title">Action </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i = 1;
                                    $bulanStrx = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                    foreach ($data['rows'] as $key => $row) :
                                        $bulan = str_split($row['bulan']);
                                        $bulan = $bulan[0] == 0 ? $bulanStrx[$bulan[1] - 1] : $bulanStrx[$row['bulan'] - 1];
                                    ?>
                                        <tr class="even pointer">
                                            <td class="a-center "><?= $i++; ?></td>
                                            <td class=" "><?= $bulan; ?></td>
                                            <td class=" "><?= $row['tahun']; ?></td>
                                            <td class=" last">
                                                <a href="#" data-toggle="modal" data-target=".modal-edit" onclick="edit('<?= $row['id']; ?>')"> Edit</a> |
                                                <a href="<?= url('spp-delete/' . $row['id']); ?>" onclick="return confirm('Yakin ingin menghapus?')">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<div class="modal fade modal-add" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="demo-form2" action="<?= url('/spp/add'); ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Tambah Data SPP</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="bulan">Bulan <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <select class="form-control" name="bulan" required="required">
                                <option value="">Pilih Bulan</option>
                                <?php
                                $bulanStrx = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                for ($bulan = 1; $bulan <= 12; $bulan++) :
                                    $bulanStr = $bulanStrx[$bulan - 1];
                                    // $bulanX = $bulan;
                                    $bulan = $bulan <= 9 ? '0' . $bulan : $bulan;
                                ?>
                                    <option value="<?= $bulan; ?>"><?= $bulanStr; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tahun">Tahun <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <select class="form-control" name="tahun" required="required">
                                <option value="">Pilih Tahun</option>
                                <?php for ($tahun = date('Y') - 3; $tahun < date('Y') + 3; $tahun++) : ?>
                                    <option value="<?= $tahun; ?>"><?= $tahun; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modal-edit" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="demo-form2" action="" method="post" data-parsley-validate class="form-horizontal form-label-left" onsubmit="return submitEdit(event)">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Ubah Data SPP</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="bulan">Bulan <span class="required">*</span>
                        </label>
                        <div class="col-lg"> 
                        <input type="hidden" class="id" value="">
                            <select class="form-control bulan" name="bulan" required="required">
                                <option value="">Pilih Bulan</option>
                                <?php
                                $bulanStrx = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                for ($bulan = 1; $bulan <= 12; $bulan++) :
                                    $bulanStr = $bulanStrx[$bulan - 1];
                                    // $bulanX = $bulan;
                                    $bulan = $bulan <= 9 ? '0' . $bulan : $bulan;
                                ?>
                                    <option value="<?= $bulan; ?>"><?= $bulanStr; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tahun">Tahun <span class="required">*</span>
                        </label>
                        <div class="col-lg">
                            <select class="form-control tahun" name="tahun" required="required">
                                <option value="">Pilih Tahun</option>
                                <?php for ($tahun = date('Y') - 3; $tahun < date('Y') + 3; $tahun++) : ?>
                                    <option value="<?= $tahun; ?>"><?= $tahun; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function edit(id)
    {  

        fetch('<?= url('spp-edit/'); ?>' + id)
        .then((res) => res.json())
        .then((data) => {
            $('.modal-edit input[type=hidden]').val(data.id);
            $('.modal-edit .bulan option[value=' + data.bulan + ']').attr('selected','selected');
            $('.modal-edit .tahun option[value=' + data.tahun + ']').attr('selected','selected');
        })
        .catch((error) => console.log(error))
    }

    function submitEdit(event)
    {
        event.preventDefault();
        id = $('.modal-edit input[type=hidden]').val();
        bulan = $('.modal-edit .bulan').val();
        tahun = $('.modal-edit .tahun').val();
        
        $.post('<?=url('spp-edit/');?>' + id,
        {
            bulan: bulan,
            tahun: tahun
        },
        function(data,status){ 
            location.reload()
        });
    }
</script>