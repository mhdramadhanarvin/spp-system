<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left pb-2">
                <h3>Tambah Data Kelas</small></h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                        <div class="x_content">
                            <?= Flasher::flash(); ?>
                            <form id="demo-form2" action="" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="bulan">Bulan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-2 col-sm-2 ">
                                        <select class="form-control" name="bulan">
                                            <option value="">Pilih Bulan</option>
                                            <?php
                                            $bulanStrx = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                            for ($bulan = 1; $bulan <= 12; $bulan++) :
                                                $bulanStr = $bulanStrx[$bulan - 1];
                                                $bulan = $bulan <= 9 ? '0' . $bulan : $bulan;
                                            ?>
                                                <option value="<?= $bulan; ?>"><?= $bulanStr; ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="tahun">Tahun <span class="required">*</span>
                                    </label>
                                    <div class="col-md-2 col-sm-2 ">
                                        <select class="form-control" name="tahun">
                                            <option value="">Pilih Tahun</option>
                                            <?php for ($tahun = date('Y') - 3; $tahun < date('Y') + 3; $tahun++) : ?>
                                                <option value="<?= $tahun; ?>"><?= $tahun; ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="item form-group">
                                    <div class="col-md-6 col-sm-6 offset-md-3">
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                        <a href="<?= url('data-spp'); ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
