<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title><?= $data['title'] ?? "Sistem Pembayaran SPP"; ?></title>

    <!-- Bootstrap -->
    <link href="<?= cdn('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= cdn('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= cdn('vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= cdn('vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?= cdn('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'); ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?= cdn('vendors/jqvmap/dist/jqvmap.min.css'); ?>" rel="stylesheet" />
    <!-- bootstrap-daterangepicker -->
    <link href="<?= cdn('vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?= cdn('build/css/custom.min.css'); ?>" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?= cdn('vendors/jquery/dist/jquery.min.js'); ?>"></script>
</head>

<body class="nav-md footer_fixed">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view" style="background: #1a334c !important;">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title"><i class="fa fa-university"></i> <span>Sistem SPP</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="<?= cdn('images/img.jpg'); ?>" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?= Session::get('user')['nama']; ?></h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a href="<?= url(); ?>"><i class="fa fa-home"></i> Halaman Utama </a></li>
                                <?php if (isset(Session::get('user')['level']) and Session::get('user')['level'] == 'admin') : ?>
                                    <li><a><i class="fa fa-database"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?= url('data-staff'); ?>">Data Staff</a></li>
                                            <li><a href="<?= url('data-student'); ?>">Data Siswa</a></li>
                                            <li><a href="<?= url('data-class'); ?>">Data Kelas</a></li>
                                            <li><a href="<?= url('data-spp'); ?>">Data SPP</a></li>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                                <li><a href="<?=url('transactions');?>"><i class="fa fa-money"></i> Transaksi Pembayaran </a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-history"></i> Riwayat Pembayaran </a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <nav class="nav navbar-nav">
                        <ul class=" navbar-right">
                            <li class="nav-item dropdown open" style="padding-left: 15px;">
                                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?= cdn('images/img.jpg'); ?>" alt=""><?= Session::get('user')['nama']; ?>
                                </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?= url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->