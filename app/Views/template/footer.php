<footer>
    <div class="pull-right">
        &copy; 2021. All Right Reserved
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<!-- <script src="<?=cdn('vendors/jquery/dist/jquery.min.js');?>"></script> -->
<!-- Bootstrap -->
<script src="<?=cdn('vendors/bootstrap/dist/js/bootstrap.bundle.min.js');?>"></script> 
<!-- NProgress -->
<script src="<?=cdn('vendors/nprogress/nprogress.js');?>"></script> 
<!-- Custom Theme Scripts -->
<script src="<?=cdn('build/js/custom.min.js');?>"></script>

</body>

</html>